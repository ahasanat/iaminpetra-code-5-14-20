<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile;

class Googlekey extends ActiveRecord
{
   
     public static function collectionName()
    {
        return 'google_key';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id','key','status','expired_on'];
    }

    public function setkey($id) {
        if($id) {
            $Googlekey = Googlekey::find()->where([(string)'_id' => $id])->one();
            if(!empty($Googlekey)) {
                Googlekey::updateAll(['status' => ''],['status' => 'active']);

                $allkeys = Googlekey::find()->where([(string)'_id' => $id])->one();
                if(!empty($allkeys)) {
                    $status = $allkeys['status'];
                    if($status != 'expired') {
                        $allkeys->status = 'active';
                        $allkeys->update();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function setkeyexpired() {
        $Googlekey = Googlekey::find()->where(['status' => 'active'])->one();
        if(!empty($Googlekey)) {
            $Googlekey->status = 'expired';
            $Googlekey->update();

            Googlekey::updateAll(['status' => ''],['status' => 'active']);

            $keydata = Googlekey::find()->where(['not', 'status', 'expired'])->one();
            if(!empty($keydata)) {
                $keydata->status = 'active';
                $keydata->update();
                return true;
            }
        }
        return false;
    }

    public function checkforunexpired() {
        $Googlekey = Googlekey::find()->where(['status' => 'expired'])->all();
        foreach ($Googlekey as $S_Googlekey) {
            $existed_time = $S_Googlekey['expired_on'];
            $time = time();

            // Formulate the Difference between two dates 
            $diff = abs($time - $expired_on);  
            $years = floor($diff / (365*60*60*24));  
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));
            //$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
            //$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
            
            if($years <= 0) {
                if($months <= 0) {
                    if($days <= 0) {
                        if($hours > 24) {
                            $Googlekey->status = '';
                            $Googlekey->expired_on = '';
                            $Googlekey->update();
                        }
                    } else {
                        $Googlekey->status = '';
                        $Googlekey->expired_on = '';
                        $Googlekey->update();
                    }
                } else {
                    $Googlekey->status = '';
                    $Googlekey->expired_on = '';
                    $Googlekey->update();
                }
            } else {
                $Googlekey->status = '';
                $Googlekey->expired_on = '';
                $Googlekey->update();
            } 
        }

        return true;
    }

    public function getkey() {
        $key = '';
        $Googlekey = Googlekey::find()->where(['status' => 'active'])->one();
        if(!empty($Googlekey)) {
            $key = $Googlekey['key'];
            return $key;
        }
        return $key;
    }

}
