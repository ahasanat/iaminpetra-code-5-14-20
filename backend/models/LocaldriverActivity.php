<?php
namespace backend\models;
use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;

use yii\web\UploadedFile; 
 
use yii\helpers\ArrayHelper;
use frontend\models\UserForm;
use frontend\models\Personalinfo;

use frontend\models\LocaldriverPostInvite;
use frontend\models\LocaldriverPostInviteMsgs;
use frontend\models\Verify;
use frontend\models\Friend;
use frontend\models\Abuse;
use frontend\models\AbuseStatement;



/**
 * This is the model class for collection "occupation".
 *
 * @property \MongoId|string $_id
 * @property mixed $post_text
 * @property mixed $post_status
 * @property mixed $post_created_date
 * @property mixed $post_user_id
 * @property mixed $image
 */

class LocaldriverActivity extends ActiveRecord
{

   /*public $imageFile1;*/
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'localdriver_activity';
    }

   /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
         return ['_id', 'name', 'created_at', 'modify_at'];

    }

    public function addactivity($uid, $name) {
        if($uid) {
            $created_at = strtotime('now');
            $LocaldriverActivity = new LocaldriverActivity();
            $LocaldriverActivity->name = $name;
            $LocaldriverActivity->created_at = $created_at;
            if($LocaldriverActivity->save()) {
                $created_at  = date("Y-m-d H:i:s", $created_at);
                $result = array('status' => true, 'name' => $name, 'created_at' => $created_at);
                return json_encode($result, true);
                exit;
            }
        }
        $result = array('status' => false);
        return json_encode($result, true);
        exit;
    }

    public function getallactivity() {
        $data = LocaldriverActivity::find()->asarray()->all();
        return json_encode($data, true);
    } 
    
}