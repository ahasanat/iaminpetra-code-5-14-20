<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
$this->title = 'Sales Statastics';
//echo "<pre>"; print_r($country); die();
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Sales</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=site/salesstatastics">sales</a></li>
		</ol> 
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-yellow">
					<div class="inner">
						<p>Total Vip Sale</p>
						<h3><?php
						if(isset($filterResult) && !empty($filterResult)) {
							if(array_key_exists('joinvip', $filterResult)) {
								echo array_sum($filterResult['joinvip']);
							}
							else
							{
								echo '0';
							}
						}
						?></h3>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-money"></i>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-green">
					<div class="inner">
						<p>Total Verify Sale</p>
						<h3><?php
						if(isset($filterResult) && !empty($filterResult)) {
							if(array_key_exists('verify', $filterResult)) {
								echo array_sum($filterResult['verify']);
							}
							else
							{
								echo '0';
							}
						}
						?></h3>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-money"></i>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-red">
					<div class="inner">
						<p>Total Credit Sale</p>
						<h3><?php
						if(isset($filterResult) && !empty($filterResult)) {
							if(array_key_exists('buycredits', $filterResult)) {
								echo array_sum($filterResult['buycredits']);
							}
							else
							{
								echo '0';
							}
						}
						?></h3>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-money"></i>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="small-box bg-blue">
					<div class="inner">
						<p>Total Ads Sale</p>
						<h3><?php
						if(isset($filterResult) && !empty($filterResult)) {
							if(array_key_exists('adds', $filterResult)) {
								echo array_sum($filterResult['adds']);
							}
							else
							{
								echo '0';
							}
						}
						?></h3>
					</div>
					<div class="icon">
						<i class="ionsize zmdi zmdi-money"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="row">	
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Order List</h3>
            </div>
			
            <!-- /.box-header -->
            <div class="box-body">
			
			<table id="sales_statastics" class="table table-bordered table-striped">
			
				<thead>
					<tr>
					<th>User Name</th>
					<th>Order Date</th>
					<th>Transaction Id</th>
					<th>Amount</th>
					<th>Detail</th>
					<th>Status</th>
					<th>Order Type</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach($record_order as $order){ 
						$result = LoginForm::find()->where(['_id' => $order['user_id']])->one();
					?>
					<tr>
					<td><?= $result['fname'];?></td>
					<td><?= $order['current_date'];?></td>
					<td><?= $order['transaction_id'];?></td>
					<td><?= $order['amount']." ".$order['curancy'];?></td>
					<td><?= $order['detail'];?></td>
					<td><?= $order['status'];?></td>
					<td><?php 
					if($order['order_type']== 'joinvip')
					{ 
						echo 'Become a Vip Member';
					}
					else if($order['order_type']== 'buycredits')
					{ 
						echo 'Purchase Credits';
					}
					else if($order['order_type']== 'verify')
					{ 
						echo 'Become a Verify Member';
					}
					else
					{ 
						echo '---';
					}
					
					?></td>

					</tr>

					<?php }
					?>

				</tbody>
			</table>
              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>