<?php 
use yii\helpers\Html;
$this->title = 'User Detail';
$uid = (string)$user['_id'];
$user_img = $this->context->getuserimageforadmin($uid,'photo'); 
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>User</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>
	

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><?= $user['fname'] ." ". $user['lname'];?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <tr>
                  <th><?= $user['fname'];?> image</th>
				  <td><img src="<?=$user_img?>" style="width: 150px; height: 150px;"/></td>
				</tr>
                <tr>
                  <th>First Name</th>
				  <td><?= $user['fname'];?></td>
				</tr>
				<tr>
                  <th>Last Name</th>
				  <td><?= $user['lname'];?></td>
				</tr>
				<tr>
                  <th>Email</th>
				  <td><?= $user['email'];?></td>
				</tr>
				<tr>
                  <th>Country</th>
				  <td>
                    <?php if($user['country'] !=''){
                        echo $user['country'];
                    }else{
                        echo "---";
                    }?></td>
			    </tr>
                  
                <tr>
                  <th>Delete</th>
				  <td><label><input type="radio" value="hard" onclick="delete_type('1')" name="delete"/>Hard </label>&nbsp;
				  <label><input type="radio" value="soft" onclick="delete_type('2')" name="delete"/>Soft </label>&nbsp;
				  <input type="hidden" value="" id="del_type"/>
				  <input type="button" onclick="delete_user('<?=$user['_id']?>')" name="ok" value="Delete"/></td>
			    </tr>
				
                <tbody>
        
                </tbody>
                
              </table>
			  
			  <script>
			  function delete_type(id){
				  $("#del_type").val(id);
			  }
				function delete_user(id){
					
					var d_type = $("#del_type").val();
					
					if(d_type !=''){ 
						
						var r = confirm("Are you sure to delete this user?");
						if (r == false) {
							return false;
						}
						else 
						{
						
							$.ajax({
								url: '?r=userdata/deleteusertype', 
								type: 'POST',
								data: 'id=' + id+ '&d_type=' +d_type,
								success: function (data) {
									
									 if(data == 1){
										 window.close();
									 }
									 
									 
								}
							});
						}
					}
					else{
						alert('please select one delete type');
						return false;
					}
				}
			</script>
			  
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
