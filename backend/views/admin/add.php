<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Add Admin User';
?>
<div class="content-wrapper">
    <section class="content-header">
		<h1>Add Admin User</h1>
	</section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
					  <h3 class="box-title">Add Admin User</h3>
					</div>
					<div class="box-body">
						<form id="frm" class="form-horizontal" method="post" action="?r=admin/addadmindata">
							<div class="box-body">
								<div class="form-group">
									<label class="col-sm-2 control-label">Username</label>
									<div class="col-sm-10">
										<input type="text" pattern=".{4,10}" class="form-control"  name="username" id="name" required/><span class="name_notice" style="display: none">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Password</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" maxlength="10" name="password"  required/><span class="name_notice" style="display: none">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Email ID</label>
									<div class="col-sm-10">
										<input type="email" class="form-control" name="email" required/><span class="name_notice" style="display: none">
									</div>
								</div>
							</div>
							<div class="box-footer">
								<input type="reset" name="clear" value="Clear"  class="btn btn-default"/>  
								<input type="submit" name="add" value="Add"  class="btn btn-info right"/>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>