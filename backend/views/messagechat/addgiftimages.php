<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Add Gift Images';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper vip-admin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Add Gift Images</h1>
     </section>

    <!-- Main content -->
    <section class="content">
    	<div class="box box-default">
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
               <div class="form-group">
	                <label class="col-sm-2">Select Category</label>
	                <div class="col-sm-5">
	                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
	                  <option selected="selected">Alabama</option>
	                  <option>Alaska</option>
	                  <option>California</option>
	                  <option>Delaware</option>
	                  <option>Tennessee</option>
	                  <option>Texas</option>
	                  <option>Washington</option>
	                </select>
	              </div>
            	</div>
            	<div style="clear:both"></div><br/> 	
            	<div class="form-group">
	                <label class="col-sm-2">Upload Images</label>
	                <div class="col-sm-5">
	                <input type="file">
	                <p class="help-block">you can upload multiple images.</p>
	              </div>
            	</div>

            	<div style="clear:both"></div><br/> 	
            	<label class="col-sm-2"></label>
            	<div class="form-group col-sm-2">
	                	<button type="button" class="btn btn-block btn-primary">Primary</button>
            	</div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
    </section>
    <!-- /.content -->

    <section class="content-header">
      <h1>Gift Images Table</h1>
     </section>

  </div>
 

    </section>
    <!-- /.content -->
  </div>


  <script type="text/javascript">

  </script>
 
