<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$cont_name = Yii::$app->controller->action->id;
$cont = Yii::$app->controller->id;
$session = Yii::$app->session;
$role = $session->get('role');
$username = $session->get('username');
?>
<aside class="main-sidebar">
    <section class="sidebar">
		<div class="user-panel">
			<div class="left image">
				<img src="<?php echo $baseUrl;?>/dist/img/user2-160x160.jpg" class="img-circle" >
			</div>
			<div class="left info">
				<p><?= ucfirst($username);?></p>
			</div>
		</div>
		<ul class="sidebar-menu">
<!-- Dashboard Menu start -->		
			<li class="treeview <?php if($cont== 'site' && $cont_name== 'index'){echo "active";}?>">
				<a href="?r=site/index">
					<i class="mdi mdi-gauge"></i> <span>Dashboard</span>
				</a>
			</li>

			<!-- Profile Administration Menu Start -->
			<?php if($role == 'superadmin'){?>
				<li class="treeview <?php if($cont == 'admin'){echo "active";};?>">
					<a href="javascript:void(0)">
						<i class="zmdi zmdi-account-secret"></i> <span>Profile Administration</span>
						<span class="right-container">
						  <i class="zmdi zmdi-chevron-left right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="<?php if($cont == 'admin'){echo "active";};?>"><a href="?r=admin/all"><i class="zmdi zmdi-circle-o"></i>Admin</a></li>
						<li><a href="?r=site/adduser"><i class="zmdi zmdi-circle-o"></i>Flagger</a></li>
					</ul>
				</li>
			<?php }?>
			
			<!-- Users Menu start -->			
			<li class="treeview <?php if(($cont == 'userdata' && $cont_name == 'index') || $cont_name == 'showvipusers' || $cont =='reference' || $cont_name == 'showverifyusers' || $cont =='search-user' || $cont =='reference'){echo "active";};?>">
				<a href="javascript:void(0)">
					<i class="zmdi zmdi-account"></i> <span>Users</span>
					<span class="right-container">
					  <i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
			    <ul class="treeview-menu">
					<li class="<?php if($cont == 'userdata' && $cont_name == 'index'){echo "active";}?>"><a href="?r=userdata/index"><i class="zmdi zmdi-circle-o"></i> Free</a></li>
					<li class="<?php if($cont_name == 'showvipusers'){echo "active";}?>"><a href="?r=site/showvipusers"><i class="zmdi zmdi-circle-o"></i> Vip</a></li>
					<li class="<?php if($cont_name == 'showverifyusers'){echo "active";}?>"><a href="?r=site/showverifyusers"><i class="zmdi zmdi-circle-o"></i> Paid</a></li>
					<li class="<?php if($cont =='reference'){echo "active";};?>"><a href="?r=reference/all"><i class="zmdi zmdi-circle-o"></i>Reference</a></li>
			    </ul>
			</li>
<!-- Settings Menu Start -->
			<li class="treeview">
				<a href="javascript:void(0)">
					<i class="zmdi zmdi-settings"></i> <span>Reported & Flagger</span>
					<span class="right-container">
						<i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li>
						<a href="javascript:void(0)"><i class="zmdi zmdi-circle-o"></i>Posts
							<span class="right-container">
								<i class="zmdi zmdi-chevron-left right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="?r=flagger/discussion"><i class="zmdi zmdi-circle-o"></i>Discussion Post</a></li>
							<li><a href="?r=flagger/photostream"><i class="zmdi zmdi-circle-o"></i>Photostream Post</a></li>
							<li><a href="?r=flagger/collections"><i class="zmdi zmdi-circle-o"></i>Collections Post</a></li>
							<li><a href="?r=flagger/trip"><i class="zmdi zmdi-circle-o"></i>Trip Post</a></li>
							<li><a href="?r=flagger/localdine"><i class="zmdi zmdi-circle-o"></i>Local Dine Post</a></li>
							<li><a href="?r=flagger/homestay"><i class="zmdi zmdi-circle-o"></i>Homestay Post</a></li>
							<li><a href="?r=flagger/camping"><i class="zmdi zmdi-circle-o"></i>Camping Post</a></li>
							<li><a href="?r=flagger/localguide"><i class="zmdi zmdi-circle-o"></i>Local Guide Post</a></li>
							<li><a href="?r=flagger/localdriver"><i class="zmdi zmdi-circle-o"></i>Local Driver Post</a></li>
							<li><a href="?r=flagger/question"><i class="zmdi zmdi-circle-o"></i>Question Post</a></li>
							<li><a href="?r=flagger/reviews"><i class="zmdi zmdi-circle-o"></i>Reviews Post</a></li>
							<li><a href="?r=flagger/tips"><i class="zmdi zmdi-circle-o"></i>Tips Post</a></li>
							<li><a href="?r=flagger/blog"><i class="zmdi zmdi-circle-o"></i>Blog Post</a></li>
							<li><a href="javascript:void(0)"><i class="zmdi zmdi-circle-o"></i>Collection Post</a></li>
							<li><a href="javascript:void(0)"><i class="zmdi zmdi-circle-o"></i>Page Post</a></li>
						</ul>
					</li>
					<li><a href="javascript:void(0)"><i class="zmdi zmdi-circle-o"></i>Photos</a></li>
					<li class="<?php if($cont_name == 'flagcollection'){echo "active";};?>"><a href="?r=site/flagcollection"><i class="zmdi zmdi-circle-o"></i>Collection</a></li>
					<li><a href="javascript:void(0)"><i class="zmdi zmdi-circle-o"></i>Pages</a></li>
					<li><a href="javascript:void(0)"><i class="zmdi zmdi-circle-o"></i>Ads</a></li>
					<li class="<?php if($cont == 'flagger' && $cont_name == 'photostream'){echo "active";}?>"><a href="?r=flagger/photostream"><i class="zmdi zmdi-circle-o"></i> Photostream</a></li>
				</ul>
			</li>
				
<!-- Post Menu start -->			
			<!--<li class="treeview <?php if($cont == 'post'){echo "active";};?>">
			  <a href="javascript:void(0)">
				<i class="zmdi zmdi-file"></i> <span>Post</span>
				<span class="right-container">
				  <i class="zmdi zmdi-chevron-left right"></i>
				</span>
			  </a>
				<ul class="treeview-menu">
					<li class="<?php if($cont_name == 'index'){echo "active";};?>"><a href="?r=post/index"><i class="zmdi zmdi-circle-o"></i> Posts</a></li>
					<li class="<?php if($cont_name == 'comments'){echo "active";};?>"><a href="?r=post/comments"><i class="zmdi zmdi-circle-o"></i> Comments</a></li>
					<li class="<?php if($cont_name == 'flagpost'){echo "active";};?>"><a href="?r=post/flagpost"><i class="zmdi zmdi-circle-o"></i>Report Abuse</a></li>
				</ul>
			</li>-->
			
<!-- Pages Menu Start -->
			<li class="treeview <?php if($cont == 'page' || $cont_name == 'addbuscat' || $cont_name == 'endorsement' || $cont_name == 'flagpage'){echo "active";};?>">
				<a href="javascript:void(0)">
					<i class="mdi mdi-file-powerpoint"></i> <span>Pages</span>
					<span class="right-container">
						<i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?php if($cont_name == 'all'){echo "active";};?>"><a href="?r=page/all"><i class="zmdi zmdi-circle-o"></i>All</a></li>
					<li class="<?php if($cont_name == 'addbuscat'){echo "active";};?>"><a href="?r=site/addbuscat"><i class="zmdi zmdi-circle-o"></i>Business Category</a></li>
					<?php /*
					<li class="<?php if($cont_name == 'endorsement'){echo "active";};?>"><a href="?r=site/endorsement"><i class="zmdi zmdi-circle-o"></i>Endorsement</a></li>
					*/ ?>
					<li class="<?php if($cont_name == 'review'){echo "active";};?>"><a href="?r=page/review"><i class="zmdi zmdi-circle-o"></i>Review Post</a></li>
					<li class="<?php if($cont_name == 'photo'){echo "active";};?>"><a href="?r=page/photo"><i class="zmdi zmdi-circle-o"></i>Photo Post</a></li>
					<li class="<?php if($cont_name == 'post'){echo "active";};?>"><a href="?r=page/post"><i class="zmdi zmdi-circle-o"></i>Post</a></li>
					<li class="<?php if($cont_name == 'image'){echo "active";};?>"><a href="?r=page/image"><i class="zmdi zmdi-circle-o"></i>Default image</a></li>
					<li class="<?php if($cont_name == 'flagpage'){echo "active";};?>"><a href="?r=site/flagpage"><i class="zmdi zmdi-circle-o"></i>Report Abuse</a></li>
				</ul>
			</li>
			
<!-- Collection Menu Start -->
			<li class="treeview <?php if($cont == 'collection'){echo "active";}?>">
				<a href="javascript:void(0)">
					<i class="mdi mdi-file-outline"></i> <span>Collection</span>
					<span class="right-container">
					  <i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?php if($cont_name == 'all'){echo "active";};?>"><a href="?r=collection/all"><i class="zmdi zmdi-circle-o"></i>All</a></li>
					<li class="<?php if($cont_name == 'post'){echo "active";};?>"><a href="?r=collection/post"><i class="zmdi zmdi-circle-o"></i>Reported post</a></li>
					<li class="<?php if($cont_name == 'image'){echo "active";};?>"><a href="?r=collection/image"><i class="zmdi zmdi-circle-o"></i>Default image</a></li>
					<li class="<?php if($cont_name == 'reportedflag'){echo "active";};?>"><a
					href="?r=collection/reportedflag"><i class="zmdi zmdi-circle-o"></i>Reported collection</a></li>
					<?php /* <li class="<?php if($cont_name == 'flagcollection'){echo "active";};?>"><a
					href="?r=site/flagcollection"><i class="zmdi zmdi-circle-o"></i>Report abuse</a></li> */?>
				</ul>
			</li>
			
<!-- Trip experience Menu Start -->
			<li class="treeview <?php if($cont =='tripexp'){echo "active";}?>">
				<a href="javascript:void(0)">
					<i class="zmdi zmdi-airplane"></i> <span>Trip experience</span>
					<span class="right-container">
					  <i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?php if($cont_name == 'all'){echo "active";};?>"><a href="?r=tripexp/all"><i class="zmdi zmdi-circle-o"></i>All</a></li>
				</ul>
			</li>
		
<!-- Travstore Menu Start -->
			<li class="treeview <?php if($cont_name == 'addtravstorecat' || $cont_name == 'travstoreimage' ){echo "active";};?>">
			  <a href="javascript:void(0)">
				<i class="fa ion-bag"></i> <span>Travstore</span>
				<span class="right-container">
				  <i class="zmdi zmdi-chevron-left right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li class="<?php if($cont_name == 'addtravstorecat'){echo "active";};?>"><a href="?r=site/addtravstorecat"><i class="zmdi zmdi-circle-o"></i> Travstore Category</a></li>
				<!--<li class="<?php if($cont_name == 'travstoreimage'){echo "active";};?>"><a href="?r=site/travstoreimage"><i class="zmdi zmdi-circle-o"></i>Default image</a></li>-->
			  </ul>
			</li>
				
<!-- Tours Menu Start -->
			<li class="treeview <?php if($cont == 'tours'){echo "active";};?>">
				<a href="javascript:void(0)">
					<i class="zmdi zmdi-car"></i> <span>Tours</span>
					<span class="right-container">
						<i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?php if($cont_name == 'all'){echo "active";};?>"><a href="?r=tours/all"><i class="zmdi zmdi-circle-o"></i>All</a></li>
					<li class="<?php if($cont_name == 'add'){echo "active";};?>"><a href="?r=tours/add"><i class="zmdi zmdi-circle-o"></i>Add</a></li>
				</ul>
			</li>

<!-- Ads Menu Start -->
			<li class="treeview <?php if($cont == 'ads'){echo "active";};?>">
				<a href="javascript:void(0)">
					<i class="zmdi zmdi-font"></i> <span>Ads</span>
					<span class="right-container">
						<i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?php if($cont_name == 'all'){echo "active";};?>"><a href="?r=ads/all"><i class="zmdi zmdi-circle-o"></i>All</a></li>
					<li class="<?php if($cont_name == 'active'){echo "active";};?>"><a href="?r=ads/active"><i class="zmdi zmdi-circle-o"></i>Active</a></li>
				</ul>
			</li>
						
<!-- Ads Places Start -->
			<li class="treeview <?php if($cont == 'places'){echo "active";}?>">
				<a href="javascript:void(0)">
					<i class="mdi mdi-sign-direction"></i> <span>Places</span>
					<span class="right-container">
						<i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?php if($cont_name == 'all'){echo "active";};?>"><a href="?r=places/all"><i class="zmdi zmdi-circle-o"></i>All</a></li>
					<li class="<?php if($cont_name == 'reviews'){echo "active";};?>"><a href="?r=places/reviews"><i class="zmdi zmdi-circle-o"></i>Reviews</a></li>
					<li class="<?php if($cont_name == 'tips'){echo "active";};?>"><a href="?r=places/tips"><i class="zmdi zmdi-circle-o"></i>Tips</a></li>
					<li class="<?php if($cont_name == 'photos'){echo "active";};?>"><a href="?r=places/photos"><i class="zmdi zmdi-circle-o"></i>Photos</a></li>
					<li class="<?php if($cont_name == 'questions'){echo "active";};?>"><a href="?r=places/questions"><i class="zmdi zmdi-circle-o"></i>Questions</a></li>
				</ul>
			</li>
			
<!-- Message Menu start -->			
			<!-- <li class="treeview">
			  <a href="?r=socket/message">
				<i class="zmdi zmdi-email"></i> <span>Message</span>
				<span class="right-container">
				  <i class="zmdi zmdi-chevron-left right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<li><a href="?r=messagechat/addgiftcategory"><i class="zmdi zmdi-circle-o"></i>Add Category</a></li>
				<li><a href="?r=messagechat/addgiftimages"><i class="zmdi zmdi-circle-o"></i>Add Gift Images</a></li>
                <li><a href="javascript:void(0)"><i class="zmdi zmdi-circle-o"></i>Update Gift Cost</a></li>
                <li><a href="?r=messagechat/addgiftimages"><i class="zmdi zmdi-circle-o"></i>Report Abuse</a></li>
			  </ul>
			</li>-->
			
<!-- Gift Menu Start -->
			<li class="treeview <?php if($cont == 'message' || $cont_name == 'updatecost'){echo "active";}?>">
			  <a href="javascript:void(0)">
				<i class="mdi mdi-gift"></i> <span>Gift</span>
				<span class="right-container">
				  <i class="zmdi zmdi-chevron-left right"></i>
				</span>
			  </a>
			   <ul class="treeview-menu">
					<li class="<?php if($cont == 'message' && $cont_name == 'updatecost'){echo "active";}?>"><a href="?r=message/updatecost"><i class="zmdi zmdi-circle-o"></i>Update gift cost</a></li>
					<li class="<?php if($cont == 'message' && $cont_name == 'images'){echo "active";}?>"><a href="?r=message/images"><i class="zmdi zmdi-circle-o"></i>Gift Images</a></li>
					<li class="<?php if($cont == 'message' && $cont_name == 'abuse'){echo "active";}?>"><a href="?r=message/abuse"><i class="zmdi zmdi-circle-o"></i>Report Abuse</a></li>
				</ul>
			</li>
			
<!-- Galleries Menu Start -->
			<li class="treeview">
			  <a href="javascript:void(0)">
				<i class="zmdi zmdi-image-o"></i> <span>Galleries</span>
				<span class="right-container">
				  <i class="zmdi zmdi-chevron-left right"></i>
				</span>
			  </a>
			</li>

<!-- Sales Menu Start -->
			<li class="treeview <?php if($cont== 'site' && $cont_name== 'vipstatastics' || $cont_name== 'verifystatastics' || $cont_name== 'free' || $cont_name== 'salesstatastics'){echo "active";}?>">
			  <a href="javascript:void(0)">
				<i class="mdi mdi-square-edit-outline"></i> <span>Sales Reports</span>
				<span class="right-container">
				  <i class="zmdi zmdi-chevron-left right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
				<!--<li><a href="?r=site/addvipplans"><i class="zmdi zmdi-circle-o"></i>Gift</a></li>-->
				<li class="<?php if($cont_name == 'vipstatastics'){echo "active";};?>"><a href="?r=site/vipstatastics"><i class="zmdi zmdi-circle-o"></i>VIP Members</a></li>
				<li class="<?php if($cont_name == 'verifystatastics'){echo "active";};?>"><a href="?r=site/verifystatastics"><i class="zmdi zmdi-circle-o"></i>Verify Members</a></li>
				<li class="<?php if($cont_name == 'free'){echo "active";};?>"><a href="?r=userdata/free"><i class="zmdi zmdi-circle-o"></i>Free Members</a></li>
				<li class="<?php if($cont_name == 'salesstatastics'){echo "active";};?>"><a href="?r=site/salesstatastics"><i class="zmdi zmdi-circle-o"></i>Sales Report</a></li>
			  </ul>
			</li>

<!-- Billing Menu Start -->
			<li class="treeview">
			  <a href="javascript:void(0)">
				<i class="mdi mdi-file-document-outline"></i> <span>Billing</span>
				<span class="right-container">
				  <i class="zmdi zmdi-chevron-left right"></i>
				</span>
			  </a>
			</li>
			<li class="treeview <?php if($cont_name == 'slider' || $cont_name == 'cover' || $cont_name == 'language' || $cont_name == 'occupation' || $cont_name == 'education' || $cont_name == 'interests' ){echo "active";};?>">
				<a href="javascript:void(0)">
					<i class="zmdi zmdi-plus"></i> <span>Account setting</span>
					<span class="right-container">
					  <i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class ="<?php if($cont_name == 'slider'){echo "active";}?>" ><a href="?r=site/slider"><i class="zmdi zmdi-circle-o"></i>Slider</a></li>
					<li class ="<?php if($cont_name == 'cover'){echo "active";}?>"><a href="?r=site/cover"><i class="zmdi zmdi-circle-o"></i>Cover</a></li>
					<li class ="<?php if($cont_name == 'language'){echo "active";}?>"><a href="?r=site/language"><i class="zmdi zmdi-circle-o"></i>Language</a></li>
					<li class ="<?php if($cont_name == 'occupation'){echo "active";}?>"><a href="?r=site/occupation"><i class="zmdi zmdi-circle-o"></i>Occupation</a></li>
					<li class ="<?php if($cont_name == 'education'){echo "active";}?>"><a href="?r=site/education"><i class="zmdi zmdi-circle-o"></i>Education</a></li>
					<li class ="<?php if($cont_name == 'interests'){echo "active";}?>"><a href="?r=site/interests"><i class="zmdi zmdi-circle-o"></i>Interests</a></li>
				</ul>
			</li>

<!-- Settings Menu Start -->
			<li class="treeview <?php if($cont== 'site' && $cont_name== 'addvipplans' || $cont_name== 'addcreditsplans' || $cont_name== 'addverifyplans'){echo "active";}?>">
			  <a href="javascript:void(0)">
				<i class="zmdi zmdi-settings"></i> <span>Settings</span>
				<span class="right-container">
				  <i class="zmdi zmdi-chevron-left right"></i>
				</span>
			  </a>
			  <ul class="treeview-menu">
					<li class="<?php if($cont_name == 'addvipplans'){echo "active";}?>"><a href="?r=site/addvipplans"><i class="zmdi zmdi-circle-o"></i>Vip Plan</a></li>
				   <li class="<?php if($cont_name == 'addverifyplans'){echo "active";}?>"><a href="?r=site/addverifyplans"><i class="zmdi zmdi-circle-o"></i>Verify Plan</a></li>
				   <li class="<?php if($cont_name == 'addcreditsplans'){echo "active";}?>"><a href="?r=site/addcreditsplans"><i class="zmdi zmdi-circle-o"></i>Credits Plan</a></li>
			 </ul>
			</li>
			
<!-- Mail Menu start -->
			<li class="treeview <?php if(Yii::$app->controller->id =='mail'){echo 'active';}?>">
				<a href="javascript:void(0)">
					<i class="zmdi zmdi-email-o"></i> <span>Mail</span>
					<span class="right-container">
					  <i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?php if($cont_name == 'index'){echo 'active';}?>"><a href="?r=mail/index"><i class="zmdi zmdi-plus"></i>Add Mail Template</a></li>
					<li class="<?php if($cont_name == 'send'){echo 'active';}?>"><a href="?r=mail/send"><i class="mdi mdi-send"></i>Send Mails</a></li>
				</ul>
			</li>
			
<!-- Chat Menu start -->			
			<li class="treeview">
			  <a href="?r=messages/chat">
				<i class="zmdi zmdi-comment"></i> <span>Chat</span>
			  </a>
			</li>

			<li class="treeview">
			  <a href="?r=google">
				<i class="zmdi zmdi-comment"></i> <span>Google key</span>
			  </a>
			</li>

			<li class="treeview">
			    <a href="javascript:void(0)">
					<i class="zmdi zmdi-email-o"></i> <span>Default Post</span>
					<span class="right-container">
					  <i class="zmdi zmdi-chevron-left right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li class="<?php if($cont == 'defaultpost' && $cont_name == 'discussion'){echo 'active';}?>"><a href="?r=defaultpost/discussion"><i class="zmdi zmdi-plus"></i>Discussion Post</a></li>
				</ul>
			</li>
		<ul>   
    </section>
</aside>