<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use frontend\models\UserForm;
use frontend\models\Vip;
use frontend\models\Verify;
use frontend\models\Order;
use backend\models\AddvipPlans;
use backend\models\AddverifyPlans;
use backend\models\AddcreditsPlans;
use backend\models\Userdata;
use backend\models\BusinessCategory;
use backend\models\TravstoreCategory;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\models\Slider;
use frontend\models\Cover;
use frontend\models\Language;
use frontend\models\Education;
use frontend\models\Interests;
use frontend\models\Occupation;
use frontend\models\Notification;
use frontend\models\Page;
use frontend\models\PlaceVisitor;
use frontend\models\PostForm;
use frontend\models\TravAds;
use frontend\models\Tours;
use backend\models\Endorsement;
use backend\models\Addgiftimages;
use backend\models\Messagechat;
use frontend\models\DefaultImage;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','slider','cover','addvipplans','removevipplans','removecreditsplans','showvipusers','addbuscat','addcreditsplans','addslider','language','education','occupation','interests','removelanguage','removeeducation','removeoccupation','removeinterests','removesubcat','removeendorsement','endorsement','adduser','addtravstorecat','removetravcat','deleteadmin','flagcollection','deletecollection','publishcollection','flagpage','deletepage','publishpage','addgiftimages','vipstatastics','verifystatastics','salesstatastics','addverifyplans','removeverifyplans','showverifyusers','profileimagecrop', 'removecover','travstoreimage','travstoreaddimage','flagperform'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],		
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $usercount = Userdata::getUserCount();
		$pagecount = count(Page::getAllPages());
		$tripexpcount = count(PostForm::getTripexpallPosts("undefined","undefined"));
		$userdashs = Userdata::getUserdash();
		$adscount = TravAds::getTotalAds();
		$photocount = PostForm::getTravPhotosCount(); 
		$postcount = PostForm::getTravPostCount();
		$tourcount = Tours::getTourListCount();
		$messagecount = "0";
		$hotelcount = rand(100,500);
		$placecount = PlaceVisitor::getPlacesCount();
        return $this->render('index',['usercounts' =>$usercount,'pagecount'=>$pagecount,'tripexpcount'=>$tripexpcount,'userdashs'=>$userdashs,'adscount' => $adscount, 'photocount'=>$photocount, 'postcount'=>$postcount, 'tourcount'=>$tourcount, 'messagecount'=>$messagecount, 'hotelcount'=>$hotelcount,'placecount'=>$placecount]);
    }
	
    public function actionCover()
    {
		$model = new Userdata();
		$cover = Cover::find()->asarray()->all();
		$usercounts = $model->getUserCount();
        
		return $this->render('cover',['cover' => $cover, 'usercounts' =>$usercounts]);
    }

    public function actionLogin()
    {
        $this->layout = 'loginLayout';
        if (!\Yii::$app->user->isGuest)
        {
           return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            //return $this->goBack();
			$session = Yii::$app->session;
			$role = Yii::$app->user->identity->admin_role;
			$username = Yii::$app->user->identity->username;
			$session->set('role',$role);
			$session->set('username',$username);
            return $this->redirect(array('site/index'));
        }
        else
        {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        exit('asfasf');
        \Yii::$app->user->logout();
        return $this->goHome(); 
    }
	
    public function actionAddvipplans()
    {
        $model = new AddvipPlans();
        $vip_plans = $model->getVipPlans();
        if(isset($_POST) && !empty($_POST))
        {
            $record = new \backend\models\AddvipPlans;
            $record->amount = (int)$_POST['amount'];
            $record->months = (int)$_POST['months'];
            $record->percentage = (float)$_POST['percentage'];
            $record->plan_type = $_POST['plan_type'];
            $record->insert();
            return true;
        }	
        return $this->render('vip',['vip_plans' =>$vip_plans]);
    }
	
	public function actionAddverifyplans()
    {
        $model = new AddverifyPlans();
        $verify_plans = $model->getVerifyPlans();
        if(isset($_POST) && !empty($_POST))
        {
            $record = new \backend\models\AddverifyPlans;
            $record->amount = (int)$_POST['amount'];
            $record->months = (int)$_POST['months'];
            $record->percentage = (float)$_POST['percentage'];
            $record->plan_type = $_POST['plan_type'];
            $record->insert();
            return true;
        }	
        return $this->render('verify',['verify_plans' =>$verify_plans]);
    }
	

    public function actionAddgiftimages()
    {
        if(isset($_SESSION['email']) && !empty($_SESSION['email'])) {
            $user_id = (string)$_SESSION['email'];
            $is_admin = Addgiftimages::IsAdmin($user_id);
            if($is_admin) {
                return $this->render('vip',['vip_plans' =>$vip_plans]);
            } else {
                \Yii::$app->user->logout();
                return $this->goHome(); 
            }
        } else {
            \Yii::$app->user->logout();
            return $this->goHome(); 
        }
        
    }
    

    public function actionAddcreditsplans()
    {
        $record = new AddcreditsPlans();
		
        $credits_plans = $record->getCreditsPlans();
        if(isset($_POST) && !empty($_POST))
        {
            $record->amount = (int)$_POST['amount'];
            $record->credits = (int)$_POST['credits'];
            $record->percentage = (float)$_POST['percentage'];
            $record->plan_type = $_POST['plan_type'];
            $record->insert();
            return true;
        }	
        return $this->render('credits',['credits_plans' =>$credits_plans]);
    }
	
    public function actionRemovevipplans()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \backend\models\AddvipPlans;
            $record = AddvipPlans::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionRemoveverifyplans()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \backend\models\AddverifyPlans;
            $record = AddverifyPlans::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionRemovecreditsplans()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \backend\models\AddcreditsPlans;
            $record = AddcreditsPlans::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
    public function actionShowvipusers()
    {
        $model =  new \frontend\models\Vip();
        $vipusers = $model->getVipusers();

        $vip = ArrayHelper::map($vipusers, 'user_id', 'user_id');

        $users =  new UserForm();
        $users = UserForm::find()->asArray()->where(['in','_id',$vip])->all();
        return $this->render('vip_users',['vipusers' =>$users]);
    }
	
	public function actionShowverifyusers()
    {
        $model =  new \frontend\models\Verify();
        $verify_users = $model->getVerifyusers();

        $vip = ArrayHelper::map($verify_users, 'user_id', 'user_id');

        $users =  new UserForm();
        $users = UserForm::find()->asArray()->where(['in','_id',$vip])->all();
        return $this->render('verify_users',['verify_users' =>$users]);
    }
    
    public function actionAddbuscat()
    {
        $model = new BusinessCategory();
        $bus_cats = $model->getBusCat();
        if(isset($_POST) && !empty($_POST))
        {
            $buscatname = ucwords($_POST['name']);
            $buscatexist = BusinessCategory::find()->where(['name' => $buscatname ])->one();
            if($buscatexist)
            {
                return 'exist';
            }
            else
            {
                $record = new \backend\models\BusinessCategory;
                $record->name = $buscatname;
                $record->insert();
                return 'insert';   
            }
        }
        return $this->render('addbuscat',['bus_cats' =>$bus_cats]);
    }
	
	
	public function actionLanguage()
    {
        $model = new Language();
        $bus_cats = $model->getBusCat();
        if(isset($_POST) && !empty($_POST))
        {
            $buscatname = ucwords($_POST['name']);
            $buscatexist = Language::find()->where(['name' => $buscatname ])->one();
            if($buscatexist)
            {
                return 'exist';
            }
            else
            {
                $record = new \frontend\models\Language;
                $record->name = $buscatname;
                $record->insert();
                return 'insert';   
            }
        }
        return $this->render('language',['bus_cats' =>$bus_cats]);
    }
	
	public function actionEducation()
    {
        $model = new Education();
        $bus_cats = $model->getBusCat();
        if(isset($_POST) && !empty($_POST))
        {
            $buscatname = ucwords($_POST['name']);
            $buscatexist = Education::find()->where(['name' => $buscatname ])->one();
            if($buscatexist)
            {
                return 'exist';
            }
            else
            {
                $record = new \frontend\models\Education;
                $record->name = $buscatname;
                $record->insert();
                return 'insert';   
            }
        }
        return $this->render('education',['bus_cats' =>$bus_cats]);
    }
	
	public function actionOccupation()
    {
        $model = new Occupation();
        $bus_cats = $model->getBusCat();
        if(isset($_POST) && !empty($_POST))
        {
            $buscatname = ucwords($_POST['name']);
            $buscatexist = Occupation::find()->where(['name' => $buscatname ])->one();
            if($buscatexist)
            {
                return 'exist';
            }
            else
            {
                $record = new \frontend\models\Occupation;
                $record->name = $buscatname;
                $record->insert();
                return 'insert';   
            }
        }
        return $this->render('occupation',['bus_cats' =>$bus_cats]);
    }
	
	public function actionInterests()
    {
        $model = new Interests();
        $bus_cats = $model->getBusCat();
        if(isset($_POST) && !empty($_POST))
        {
            $buscatname = ucwords($_POST['name']);
            $buscatexist = Interests::find()->where(['name' => $buscatname ])->one();
            if($buscatexist)
            {
                return 'exist';
            }
            else
            {
                $record = new \frontend\models\Interests;
                $record->name = $buscatname;
                $record->insert();
                return 'insert';   
            }
        }
        return $this->render('interests',['bus_cats' =>$bus_cats]);
    }
	
	public function actionEndorsement()
    {
        $model = new Endorsement();
        $bus_cats = $model->getEndorsement();
        if(isset($_POST) && !empty($_POST))
        {
            $buscatname = ucwords($_POST['name']);
            $buscatexist = Endorsement::find()->where(['name' => $buscatname ])->one();
            if($buscatexist)
            {
                return 'exist';
            }
            else
            {
                $record = new \backend\models\Endorsement;
                $record->name = $buscatname;
                $record->insert();
                return 'insert';   
            }
        }
        return $this->render('endorse',['bus_cats' =>$bus_cats]);
    }
	
    public function actionRemovelanguage()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \frontend\models\Language;
            $record = Language::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionRemoveendorsement()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \backend\models\Endorsement;
            $record = Endorsement::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionRemoveeducation()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \frontend\models\Education;
            $record = Education::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionRemoveoccupation()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \frontend\models\Occupation;
            $record = Occupation::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionRemoveinterests()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \frontend\models\Interests;
            $record = Interests::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionRemovesubcat()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \backend\models\BusinessCategory;
            $record = BusinessCategory::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionAdduser()
    {
		if(isset($_POST) && !empty($_POST))
		{
			$record = Userdata::find()->where(['email' => $_POST['email']])->one();
			if($record)
			{
				return 0;
			}
			else
			{
				$fname = $_POST['fname'];
				$lname = $_POST['lname'];
				$email = strtolower($_POST['email']);
				$password = $_POST['password'];
				$date = time();
				
				$signup = new UserForm();
				$signup->fname = $fname;
				$signup->lname = $lname;
				$signup->email = $email;
				$signup->password = $password;
				$signup->con_password = $password;
				$signup->status = "10";
				$signup->created_date = "$date";
				$signup->updated_date = "$date";
				$signup->created_at = $date;
				$signup->updated_at = $date;
				$signup->fullname = $fname . " " . $lname;
				$signup->login_from_ip = $_SERVER['REMOTE_ADDR'];
				$signup->insert();

				return 1;
			}			
		}
		$model = new Userdata();
        $userlists = $model->getFrontadmin();
        return $this->render('addnewuser',['userdatas' => $userlists]);
	}
	
	
	public function actionDeleteadmin()
    {
		if(isset($_POST) && !empty($_POST))
		{
			$record = Userdata::find()->where(['_id' => $_POST['id']])->one();
			
			if($record)
			{
				$record->delete();
				return 1;
			}
			else
			{
				return 0;
			}			
		}
		
	}
	
	 public function actionAddtravstorecat()
    {
	   $model = new TravstoreCategory();
        $trav_cats = $model->getTravCat();
        if(isset($_POST) && !empty($_POST))
        {
            $travcatname = ucwords($_POST['name']);
            $travcatexist = TravstoreCategory::find()->where(['name' => $travcatname ])->one();
            if($travcatexist)
            {
                return 'exist';
            }
            else
            {
                $record = new \backend\models\TravstoreCategory;
                $record->name = $travcatname;
                $record->insert();
                return 'insert';   
            }
        }
        return $this->render('addtravstorecat',['trav_cats' =>$trav_cats]);
    }
	
	public function actionRemovetravcat()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
            $id = $_POST['id'];
            $record = new \backend\models\TravstoreCategory;
            $record = TravstoreCategory::find()->where(['_id' => $id])->one();			
            $record->delete();
            return true;
        }
    }
	
	public function actionFlagpage()
    {
        $model = new Page();
        $pagelists = $model->getFlagpage();
		return $this->render('flagpage',['pagelists' => $pagelists]);
    }
	
	public function actionDeletepage()
	{
		$session = Yii::$app->session;
        
		$page_id = $_POST['page_id'];
		$date = time(); 
		$update = Page::find()->where(['page_id' => "$page_id"])->one();
		$update->is_deleted = "0";
		$update->updated_date = "$date";
		$update->update();
		
		return true;
	}
	
	public function actionPublishpage()
	{
		$session = Yii::$app->session;
		$page_id = $_POST['page_id'];
		$date = time(); 
		$update = Page::find()->where(['page_id' => "$page_id"])->one();
		$update->is_deleted = "1";
		$update->updated_date = "$date";
		$update->update();
		
		$page_owner_id = $update['created_by'];
	
		$notification =  new Notification();
		$notification->post_id = "$page_id";
		$notification->user_id = "$page_owner_id";
		$notification->notification_type = 'publishpage';
		$notification->is_deleted = '0';
		$notification->status = '1';
		$notification->created_date = "$date";
		$notification->updated_date = "$date";
		$notification->insert();
		
		return true;
	}
	
	public function actionVipstatastics()
    {
        $model =  new \frontend\models\Vip();
        $vipusers = $model->getVipusers();

        $vip = ArrayHelper::map($vipusers, 'user_id', 'user_id');

        $users =  new UserForm();
		
        $country = ArrayHelper::map(UserForm::find()->asArray()->where(['in','_id',$vip])->all(),function($scope) { return (string)$scope['_id'];}, 'country');
		
		$gender = ArrayHelper::map(UserForm::find()->asArray()->where(['in','_id',$vip])->all(),function($scope) { return (string)$scope['_id'];}, 'gender');
        
		$country = array_count_values($country);
		$gender = array_count_values($gender);
		
        return $this->render('vip_statastics',['country' =>$country,'gender' => $gender]);
    }
	
	public function actionVerifystatastics()
    {
        $model =  new \frontend\models\Verify();
        $verifyusers = $model->getVerifyusers();

        $verify = ArrayHelper::map($verifyusers, 'user_id', 'user_id');

        $users =  new UserForm();
		
        $country = ArrayHelper::map(UserForm::find()->asArray()->where(['in','_id',$verify])->all(),function($scope) { return (string)$scope['_id'];}, 'country');
		
        $gender = ArrayHelper::map(UserForm::find()->asArray()->where(['in','_id',$verify])->all(),function($scope) { return (string)$scope['_id'];}, 'gender');
        
		$country = array_count_values($country);
		$gender = array_count_values($gender);
		
        return $this->render('verify_statastics',['country' =>$country, 'gender' => $gender]);
    }
	
	public function actionSalesstatastics()
    {
        $model =  new \frontend\models\Order();
		
		$selectorBulk = array('joinvip','verify','buycredits','adds');
		$record_order = Order::find()->where(['in', 'order_type', $selectorBulk])->orderBy(['current_date'=>SORT_DESC])->asarray()->all();
		
		$filterResult = array();
		foreach($record_order as $key => $values) {
			$amount = $values['amount'];
			$orderType = $values['order_type'];
			$filterResult[$orderType][] = $amount;
		}
		
        return $this->render('sales_statastics',['filterResult' =>$filterResult,'record_order' => $record_order]);
    }
	
	public function actionProfileimagecrop()
    {        
		$rand = time();
		$imgUrl = $_POST['imgUrl'];
		// original sizes
		$imgInitW = $_POST['imgInitW'];
		$imgInitH = $_POST['imgInitH'];
		// resized sizes
		$imgW = $_POST['imgW'];
		$imgH = $_POST['imgH'];
		// offsets
		$imgY1 = $_POST['imgY1'];
		$imgX1 = $_POST['imgX1'];
		// crop box
		$cropW = $_POST['cropW'];
		$cropH = $_POST['cropH'];
		// rotation angle
		$angle = $_POST['rotation'];

		$jpeg_quality = 100;

		$output_filename = "../frontend/web/uploads/cover/thumbs/thumb_".$rand;

		$what = getimagesize($imgUrl);

		switch(strtolower($what['mime']))
		{
			case 'image/png':
				$img_r = imagecreatefrompng($imgUrl);
				$source_image = imagecreatefrompng($imgUrl);
				$type = '.png';
				break;
			case 'image/jpeg':
				$img_r = imagecreatefromjpeg($imgUrl);
				$source_image = imagecreatefromjpeg($imgUrl);
				error_log("jpg");
				$type = '.jpeg';
				break;
			case 'image/gif':
				$img_r = imagecreatefromgif($imgUrl);
				$source_image = imagecreatefromgif($imgUrl);
				$type = '.gif';
				break;
			default: die('image type not supported');
		}
	
		// resize the original image to size of editor
		$resizedImage = imagecreatetruecolor($imgW, $imgH);
		imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
		// rotate the rezized image
		$rotated_image = imagerotate($resizedImage, -$angle, 0);
		// find new width & height of rotated image
		$rotated_width = imagesx($rotated_image);
		$rotated_height = imagesy($rotated_image);
		// diff between rotated & original sizes
		$dx = $rotated_width - $imgW;
		$dy = $rotated_height - $imgH;
		// crop rotated image to fit into original rezized rectangle
		$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
		imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
		imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
		// crop image into selected area
		$final_image = imagecreatetruecolor($cropW, $cropH);
		imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
		imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
		// finally output png image
		//imagepng($final_image, $output_filename.$type, $png_quality);
		imagejpeg($final_image, $output_filename.$type, $jpeg_quality);

		// Store For Original Image..
		$imageDataEncoded = base64_encode(file_get_contents($imgUrl));
		$imageData = base64_decode($imageDataEncoded);
		$source = imagecreatefromstring($imageData);
		$imageSave = imagejpeg($source, '../frontend/web/uploads/cover/'.$rand.$type,100);
		imagedestroy($source);
		
		$date =time();
		$post =new Cover();
		$post->cover_image = $rand.$type;
		$post->created_at = strtotime('now');
		if($post->save()) {
			$data = Cover::find()->orderBy('created_at DESC')->asarray()->one();
			if(!empty($data)) {
				$id = (string)$data['_id'];
				$response = Array("status" => true, 'url' => '../frontend/web/uploads/cover/'.$rand.$type, 'id' => $id);
				return json_encode($response, true);
				exit;
			}
			
		}        
    }
	
	 public function actionRemovecover() 
	 {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) 
		{
            if(isset($_POST['$id']) && $_POST['$id'] != '') 
			{
                $id = $_POST['$id'];
                $cover = Cover::find()->where([(string)'_id' => $id])->asarray()->one();
				$image_name = $cover['cover_image'];
				
				$againCover = Cover::findOne($id);
				if($againCover->delete()) 
				{
					if(file_exists('../frontend/web/uploads/cover/'.$image_name)) 
					{
						unlink('../frontend/web/uploads/cover/'.$image_name);
						unlink('../frontend/web/uploads/cover/thumbs/thumb_'.$image_name);
					}
					return true;
					exit;
				}
            }
        }
		return false;
		exit;
    } 

	public function actionTravstoreimage()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$defaultimage = DefaultImage::find()->where(['type'=>"travstore"])->one();
			return $this->render('travstoreimage',['defaultimage' =>$defaultimage]);	
		}	
	}
	
	public function actionTravstoreaddimage()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome(); 
		} else {
			$image = "travitem-default.png";
			$addimage = new DefaultImage();
			$addimage->type = "travstore";
			$addimage->image = $image;
			$addimage->insert();
		}
	}	   
}
