<?php
    // live
    // $m = new MongoClient("mongodb://localhost", array("username" => 'adel', "password" => 'hasanat'));
    
    // local
    $m = new MongoClient();
    
    $db = $m->selectDB("travbud");
    $UserPhotos = $db->user_photos->find(
        /*['post_status' => '1'],
        ['projection' => ['_id' => 1]]*/
    );

    $imagesBULK = array();
    $eximgs = array();
    if(!empty($UserPhotos)) {
        foreach ($UserPhotos as $S_UserPhotos) {
            $images = $S_UserPhotos['image'];
            $images = explode(',', $images);
            $images = array_values(array_filter($images));
            $imagesBULK = array_merge($imagesBULK, $images);
        }
    }

    $imagesBULK = array_values(array_filter($imagesBULK));

    $totalImage = count($imagesBULK);

    if($totalImage >7) {
        $imagesBULK = array_slice($imagesBULK,0,7);
    }

    $deleteAllRecords = $db->general->remove(['label' => 'photostreamcount']);

    $insertRecord = $db->general->insert([
        'label' => 'photostreamcount',
        'count' => $totalImage,
        'images' => $imagesBULK
    ]);

?>