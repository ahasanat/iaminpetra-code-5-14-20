<?php
namespace api\modules\v1\models;
use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for collection "user_post".
 *
 * @property \MongoId|string $_id
 * @property mixed $post_text
 * @property mixed $post_status
 * @property mixed $post_created_date
 * @property mixed $post_user_id
 * @property mixed $image
 */

class Notification extends ActiveRecord
{
  
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'notification';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'user_id', 'post_id','post_owner_id','shared_by','share_id','from_connect_id', 'comment_content','comment_id','reply_comment_id','like_id','page_id','notification_type','status','created_date','updated_date','ip','is_deleted','review_setting','entity','page_id','flag_reason','page_role_type','collection_owner_id'];
    }
   
    /**
     * User model relations
     */
    public function getUser()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'user_id']);
    }// Post has_one user.
    
    /**
     * Post model relations
     */
    public function getPost()
    {
        return $this->hasOne(PostForm::className(), ['_id' => 'post_id']);
    }// User has_many Posts.
    
     /**
     * Like model relations
     */
    public function getLike()
    {
        return $this->hasMany(Like::className(), ['_id' => 'like_id']);
    }
    
    /**
     * Comment model relations
     */
    public function getComment()
    {
        return $this->hasMany(Comment::className(), ['_id' => 'comment_id']);
    }
     /**
     * Get all user notification in decsending order
     * @param  nill
     * @return array
     * @Author  Sonali Patel
     * @Date    02-03-2016 (dd-mm-yyyy)
     */ 
     public function getUserPostBudge()
     {
        $session = Yii::$app->session;
        $uid = (string)$session->get('user_id');
        
        $connections =  Connect::getuserConnections($uid);
        $ids = array();
        foreach($connections as $connect)
        {
            $ids[]= $connect['from_id'];
        }
        $mute_ids = MuteConnect::getMuteconnectionsIds($uid);
        $mute_connect_ids =  (explode(",",$mute_ids['mute_ids']));
        
        //Note ::: here date condition is pending 
        $login_user = LoginForm::find()->where(['_id' => "$uid"])->one();
        
        $view_noti_time =  $login_user->last_login_time;
        
        $notification_settings = NotificationSetting::find()->where(['user_id' => (string)$uid])->one();
        
        if($notification_settings)
        {
            if($notification_settings['connect_activity'] == 'Yes')
            {
        
                // $notification = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->orderBy(['created_date'=>SORT_DESC])->all();

                //$notification = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

                $notificationpost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->andwhere(['not','notification_type','tag_connect'])->andwhere(['not','notification_type','connectrequestaccepted'])->andwhere(['not','notification_type','connectrequestdenied'])->andwhere(['not','notification_type','comment'])->andwhere(['not','notification_type','likepost'])->andwhere(['not','notification_type','likecomment'])->andwhere(['not','notification_type','commentreply'])->andwhere(['not','notification_type','sharepost'])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

                $notificationtag = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'tag_connect','user_id'=>(string)$uid])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                if($notification_settings['connect_request'] == 'Yes')
                {
                    $notificationconnectaccepted = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestaccepted','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationconnectdenied = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestdenied','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
                }
                else
                {
                    $notificationconnectaccepted = array();
                    $notificationconnectdenied = array();
                }

                if($notification_settings['connect_activity_on_user_post'] == 'Yes')
                {
                    $notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationlikecomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likecomment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationcommentreply = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'commentreply','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
                    
                    $notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
                    
                    $notificationsharepostowner = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();
                }
                else
                {
                    $notificationcomment = array();
                    $notificationlikepost = array();
                    $notificationlikecomment = array();
                    $notificationcommentreply = array();
                    $notificationsharepost = array();
                    $notificationsharepostowner = array();
                }

                $notification = array_merge_recursive($notificationpost,$notificationtag,$notificationconnectaccepted,$notificationconnectdenied,$notificationcomment,$notificationlikepost,$notificationlikecomment,$notificationcommentreply,$notificationsharepost,$notificationsharepostowner);

                return count($notification);
            }
            else
            {
                return 0;
            }
        }
        else
        {
            // $notification = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->orderBy(['created_date'=>SORT_DESC])->all();

            //$notification = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationpost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->andwhere(['not','notification_type','tag_connect'])->andwhere(['not','notification_type','connectrequestaccepted'])->andwhere(['not','notification_type','connectrequestdenied'])->andwhere(['not','notification_type','comment'])->andwhere(['not','notification_type','likepost'])->andwhere(['not','notification_type','likecomment'])->andwhere(['not','notification_type','commentreply'])->andwhere(['not','notification_type','sharepost'])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationtag = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'tag_connect','user_id'=>(string)$uid])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationconnectaccepted = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestaccepted','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationconnectdenied = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestdenied','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationlikecomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likecomment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationcommentreply = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'commentreply','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationsharepostowner = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->andwhere(['created_date'=> ['$gte'=>"$view_noti_time"]])->orderBy(['created_date'=>SORT_DESC])->all();

            $notification = array_merge_recursive($notificationpost,$notificationtag,$notificationconnectaccepted,$notificationconnectdenied,$notificationcomment,$notificationlikepost,$notificationlikecomment,$notificationcommentreply,$notificationsharepost,$notificationsharepostowner);

            return count($notification);
        }
     }
    
    
    /**
     * Get all user notification in decsending order
     * @param  nill
     * @return array
     * @Author  Sonali Patel
     * @Date    02-03-2016 (dd-mm-yyyy)
    */ 
    public function getAllNotification()
    {
        $session = Yii::$app->session;
        $uid = (string)$session->get('user_id');
        $connections =  Connect::getuserConnections($uid);
        $ids = array();
        foreach($connections as $connect)
        {
            $ids[]= $connect['from_id'];
        }
        $mute_ids = MuteConnect::getMuteconnectionsIds($uid);
        $mute_connect_ids =  (explode(",",$mute_ids['mute_ids']));
        
        /*$hide_ids = HideNotification::getHideNotifications($uid);
        $hide_not_ids =  (explode(",",$hide_ids['notification_ids']));*/
        
        $notification_settings = NotificationSetting::find()->where(['user_id' => (string)$uid])->one();
        
        if($notification_settings)
        {
            if($notification_settings['connect_activity'] == 'Yes')
            {
                //$notification = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->orderBy(['created_date'=>SORT_DESC])->all();

                $notificationpost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->andwhere(['not','notification_type','tag_connect'])->andwhere(['not','notification_type','connectrequestaccepted'])->andwhere(['not','notification_type','connectrequestdenied'])->andwhere(['not','notification_type','comment'])->andwhere(['not','notification_type','likepost'])->andwhere(['not','notification_type','likecomment'])->andwhere(['not','notification_type','commentreply'])->andwhere(['not','notification_type','sharepost'])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                $notificationtag = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'tag_connect','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                if($notification_settings['connect_request'] == 'Yes')
                {
                    $notificationconnectaccepted = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestaccepted','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationconnectdenied = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestdenied','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                }
                else
                {
                    $notificationconnectaccepted = array();
                    $notificationconnectdenied = array();
                }
                
                if($notification_settings['connect_activity_on_user_post'] == 'Yes')
                {
                    $notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationlikecomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likecomment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationcommentreply = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'commentreply','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                    
                    $notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                    
                    $notificationsharepostowner = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                }
                else
                {
                    $notificationcomment = array();
                    $notificationlikepost = array();
                    $notificationlikecomment = array();
                    $notificationcommentreply = array();
                    $notificationsharepost = array();
                    $notificationsharepostowner = array();
                }

                $notification = array_merge_recursive($notificationpost,$notificationtag,$notificationconnectaccepted,$notificationconnectdenied,$notificationcomment,$notificationlikepost,$notificationlikecomment,$notificationcommentreply,$notificationsharepost,$notificationsharepostowner);
                foreach ($notification as $key)
                {
                    $sortkeys[] = $key["created_date"];
                }

                if(count($notification))
                {
                    array_multisort($sortkeys, SORT_DESC, SORT_STRING, $notification);
                }

                return $notification;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            //$notification = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationpost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->andwhere(['not','notification_type','tag_connect'])->andwhere(['not','notification_type','connectrequestaccepted'])->andwhere(['not','notification_type','connectrequestdenied'])->andwhere(['not','notification_type','comment'])->andwhere(['not','notification_type','likepost'])->andwhere(['not','notification_type','likecomment'])->andwhere(['not','notification_type','commentreply'])->andwhere(['not','notification_type','sharepost'])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationtag = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'tag_connect','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationconnectaccepted = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestaccepted','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationconnectdenied = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestdenied','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationlikecomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likecomment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationcommentreply = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'commentreply','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
            $notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationsharepostowner = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notification = array_merge_recursive($notificationpost,$notificationtag,$notificationconnectaccepted,$notificationconnectdenied,$notificationcomment,$notificationlikepost,$notificationlikecomment,$notificationcommentreply,$notificationsharepost,$notificationsharepostowner);
            foreach ($notification as $key)
            {
                $sortkeys[] = $key["created_date"];
            }

            if(count($notification))
            {
                array_multisort($sortkeys, SORT_DESC, SORT_STRING, $notification);
            }

            return $notification;
        }
    }
    
    public function getAllNotificationAPI($uid)
    {
        $connections =  Connect::getuserConnections($uid);
        $ids = array();
        foreach($connections as $connect)
        {
            $ids[]= $connect['from_id'];
        }
        $mute_ids = MuteConnect::getMuteconnectionsIds($uid);
        $mute_connect_ids =  (explode(",",$mute_ids['mute_ids']));
        
        /*$hide_ids = HideNotification::getHideNotifications($uid);
        $hide_not_ids =  (explode(",",$hide_ids['notification_ids']));*/
        
        $notification_settings = NotificationSetting::find()->where(['user_id' => (string)$uid])->one();
        
        if($notification_settings)
        {
            if($notification_settings['connect_activity'] == 'Yes')
            {
                //$notification = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->orderBy(['created_date'=>SORT_DESC])->all();

                $notificationpost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->andwhere(['not','notification_type','tag_connect'])->andwhere(['not','notification_type','page_role_type'])->andwhere(['not','notification_type','comment'])->andwhere(['not','notification_type','likepost'])->andwhere(['not','notification_type','pagereview'])->andwhere(['not','notification_type','onpagewall'])->andwhere(['not','notification_type','likecomment'])->andwhere(['not','notification_type','commentreply'])->andwhere(['not','notification_type','sharepost'])->andwhere(['not','notification_type','pageinvite'])->andwhere(['not','notification_type','pageinvitereview'])->andwhere(['not','notification_type','likepage'])->andwhere(['not','notification_type','deletepostadmin'])->andwhere(['not','notification_type','deletecollectionadmin'])->andwhere(['not','notification_type','deletepageadmin'])->andwhere(['not','notification_type','editpostuser'])->andwhere(['not','notification_type','editcollectionuser'])->andwhere(['not','notification_type','publishpost'])->andwhere(['not','notification_type','publishcollection'])->andwhere(['not','notification_type','publishpage'])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                $notificationtag = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'tag_connect','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                if($notification_settings['connect_request'] == 'Yes')
                {
                    $notificationconnectaccepted = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestaccepted','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationconnectdenied = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestdenied','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                }
                else
                {
                    $notificationconnectaccepted = array();
                    $notificationconnectdenied = array();
                }
                
                if($notification_settings['connect_activity_on_user_post'] == 'Yes')
                {
                    $notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationlikecomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likecomment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                    $notificationcommentreply = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'commentreply','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                    
                    $notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                    
                    $notificationsharepostowner = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                }
                else
                {
                    $notificationcomment = array();
                    $notificationlikepost = array();
                    $notificationlikecomment = array();
                    $notificationcommentreply = array();
                    $notificationsharepost = array();
                    $notificationsharepostowner = array();
                }
                $notificationpageinvitation = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pageinvite','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                
                $notificationpageinvitationreview = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pageinvitereview','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
                $notificationpagelikes = Notification::find()->with('user')->with('like')->where(['notification_type'=>'likepage','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                
                $notificationpagereviews = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pagereviews','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                
                $notificationadmindelete = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletepostadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
				$n_flag_collection_adminn = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletecollectionadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
				$n_flag_page_adminn = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletepageadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
				$notificationeditpost = Notification::find()->with('user')->with('like')->where(['notification_type'=>'editpostuser','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
				$n_editcollection = Notification::find()->with('user')->with('like')->where(['notification_type'=>'editcollectionuser','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

                $notificationpublishpost = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishpost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
				$n_publish_collection3 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishcollection','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
				$n_publish_page3 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishpage','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
				
				$notificationpageroles = Notification::find()->with('user')->with('like')->where(['notification_type'=>'page_role_type','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
                
                $notificationpagewall = Notification::find()->with('user')->with('like')->where(['notification_type'=>'onpagewall','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
	
                $notification = array_merge_recursive($notificationpost,$notificationtag,$notificationconnectaccepted,$notificationconnectdenied,$notificationcomment,$notificationlikepost,$notificationlikecomment,$notificationcommentreply,$notificationsharepost,$notificationsharepostowner,$notificationpageinvitation,$notificationpageinvitationreview,$notificationpagelikes,$notificationpagereviews,$notificationadmindelete,$notificationeditpost,$notificationpublishpost,$notificationpageroles,$notificationpagewall,$n_flag_collection_adminn,$n_editcollection,$n_publish_collection3,$n_flag_page_adminn,$n_publish_page3);
                foreach ($notification as $key)
                {
                    $sortkeys[] = $key["created_date"];
                }

                if(count($notification))
                {
                    array_multisort($sortkeys, SORT_DESC, SORT_STRING, $notification);
                }

                return $notification;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            //$notification = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationpost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['in','user_id',$ids])->andwhere(['not in','user_id',$mute_connect_ids])->andwhere(['not','notification_type','tag_connect'])->andwhere(['not','notification_type','page_role_type'])->andwhere(['not','notification_type','connectrequestaccepted'])->andwhere(['not','notification_type','connectrequestdenied'])->andwhere(['not','notification_type','comment'])->andwhere(['not','notification_type','onpagewall'])->andwhere(['not','notification_type','likepost'])->andwhere(['not','notification_type','likecomment'])->andwhere(['not','notification_type','commentreply'])->andwhere(['not','notification_type','sharepost'])->andwhere(['not','notification_type','pageinvite'])->andwhere(['not','notification_type','pageinvitereview'])->andwhere(['not','notification_type','pagereview'])->andwhere(['not','notification_type','likepage'])->andwhere(['not','notification_type','onwall'])->andwhere(['not','notification_type','deletepostadmin'])->andwhere(['not','notification_type','deletecollectionadmin'])->andwhere(['not','notification_type','deletepageadmin'])->andwhere(['not','notification_type','editpostuser'])->andwhere(['not','notification_type','editcollectionuser'])->andwhere(['not','notification_type','publishpost'])->andwhere(['not','notification_type','publishcollection'])->andwhere(['not','notification_type','publishpage'])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationtag = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'tag_connect','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationconnectaccepted = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestaccepted','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationconnectdenied = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'connectrequestdenied','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationcomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'comment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationlikepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationlikecomment = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'likecomment','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationcommentreply = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'commentreply','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
            $notificationsharepost = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationsharepostowner = Notification::find()->with('user')->with('post')->with('comment')->with('like')->where(['notification_type'=>'sharepost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
            $notificationpageinvitation = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pageinvite','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
            $notificationpageinvitationreview = Notification::find()->with('user')->with('like')->where(['notification_type'=>'pageinvitereview','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
            $notificationpagelikes = Notification::find()->with('user')->with('like')->where(['notification_type'=>'likepage','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationpagereviews = Notification::find()->with('user')->where(['notification_type'=>'pagereview','from_connect_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
            $notificationontimeline = Notification::find()->with('user')->with('like')->where(['notification_type'=>'onwall','shared_by'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationadmindelete = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletepostadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$no_flag_collection_admin = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletecollectionadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$no_flag_page_admin = Notification::find()->with('user')->with('like')->where(['notification_type'=>'deletepageadmin','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$notificationeditpost = Notification::find()->with('user')->with('like')->where(['notification_type'=>'editpostuser','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_editcollection2 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'editcollectionuser','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationpublishpost = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishpost','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_publish_collection4 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishcollection','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
			$n_publish_page4 = Notification::find()->with('user')->with('like')->where(['notification_type'=>'publishpage','user_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
			
            $notificationpageroles = Notification::find()->with('user')->with('like')->where(['notification_type'=>'page_role_type','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();

            $notificationpagewall = Notification::find()->with('user')->with('like')->where(['notification_type'=>'onpagewall','post_owner_id'=>(string)$uid])->andwhere(['is_deleted'=>"0"])->orderBy(['created_date'=>SORT_DESC])->all();
            
            $notification = array_merge_recursive($notificationpost,$notificationtag,$notificationconnectaccepted,$notificationconnectdenied,$notificationcomment,$notificationlikepost,$notificationlikecomment,$notificationcommentreply,$notificationsharepost,$notificationsharepostowner,$notificationpageinvitation,$notificationpagelikes,$notificationpageinvitationreview,$notificationontimeline,$notificationpagereviews,$notificationadmindelete,$notificationeditpost,$notificationpublishpost,$notificationpageroles,$notificationpagewall,$no_flag_collection_admin,$n_editcollection2,$n_publish_collection4,$no_flag_page_admin,$n_publish_page4);
			
            foreach ($notification as $key)
            {
                $sortkeys[] = $key["created_date"];
            }

            if(count($notification))
            {
                array_multisort($sortkeys, SORT_DESC, SORT_STRING, $notification);
            }

            return $notification;
        }
    }
    
}